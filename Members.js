const members = [
    {
        id: 1,
        name: 'martin',
        status: 'ok'
    },
    {
        id: 2,
        name: 'jean',
        status: 'ok'
    },
    {
        id: 3,
        name: 'eric',
        status: 'not ok'
    },
]

module.exports = members;
