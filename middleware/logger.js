const moment = require('moment');

const logger = (req, res, next) => { //every time we make a requests this will run
    console.log(`${req.protocol}://${req.get('host')}${req.originalUrl}:${moment().format()}`);
    console.log('test');
    next()
}

module.exports = logger;
