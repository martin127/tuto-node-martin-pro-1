const express = require('express');
const router = express.Router();
const members = require('../../Members')
const uuid = require('uuid')


//Récupere tous les membres
router.get('/',(req, res) => {
    res.json(members);
})

//Récupérer un membre spécifique en fonction de son id
router.get('/:id', (req, res) => {
    const found = members.some(member => member.id === parseInt(req.params.id))

    if(found){
        res.json(members.filter(members => members.id === parseInt(req.params.id)))
    } else{
        res.status(400).json({msg: `Le membre avec l'id ${req.params.id} n'existe pas !`})
    }
})

//Create Members
router.post('/', (req, res) => {
    const newMember = {
        id: uuid.v4(),
        name: req.body.name,
        status: 'ok'
    }

    if(!newMember.name) {
       return res.status(400).json({msg: 'Veuillez rentrer un nom'})
    }

    members.push(newMember);
    //res.json(members)

    res.redirect('/')

})


//Update Members
router.put('/:id', (req, res) => {
    const found = members.some(member => member.id === parseInt(req.params.id))

    if(found){
        const updMember = req.body
        members.forEach(member => {
            if(member.id === parseInt(req.params.id)){
                member.name = updMember.name ? updMember.name : member.name

                res.json({msg: 'member updated ', member})
            }
        })
    } else{
        res.status(400).json({msg: `Le membre avec l'id ${req.params.id} n'existe pas !`})
    }
})


//Delete a member
router.delete('/:id', (req, res) => {
    const found = members.some(member => member.id === parseInt(req.params.id))

    if(found){

        res.json({msg : `Le membre avec l'id ${req.params.id} a bien été supprimé !`, members: members.filter(members => members.id !== parseInt(req.params.id))})
    } else{
        res.status(400).json({msg: `Le membre avec l'id ${req.params.id} n'existe pas !`})
    }
})


module.exports = router;