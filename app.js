const express = require('express')
const app = express()
const path = require('path')
const PORT = process.env.PORT || 3000
const logger = require('./middleware/logger')
const exphbs = require('express-handlebars')
const members = require('./Members')

//app.get('/test', (req, res) => {
  //res.sendFile(path.join(__dirname, 'public', 'index.html'))
//})

//  Body Parser Middleware
app.use(express.json())
app.use(express.urlencoded({ extended: false} ));


//  HomePage Router
app.get('/', (req, res) => res.render('index', {
  title: 'Members',
  members
}))

//  Init middleware
//app.use(logger)

// HandlerBars Middleware
app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

//  Set static folder
//app.use(express.static(path.join(__dirname, 'public')))// va creer des routes automatiquement pour tous les fichiers qu'on aura dans notre dossier public, ex: si on a about.html, cela nous créra la route /about.html et affichera la page about.html

//  Members API Route
app.use('/api/members', require('./routes/api/members'))



app.listen(PORT, function () {
  console.log(`Example app listening on port ${PORT}`)
})


